package org.miquel.pushmessage.adapter;

import android.app.Activity;
import android.content.Intent;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.miquel.pushmessage.R;
import org.miquel.pushmessage.activities.DetailMessage_Activity;
import org.miquel.pushmessage.activities.ListMessages_Activity;
import org.miquel.pushmessage.model.Message;
import org.miquel.pushmessage.realmDAO.MessageDAO;
import org.parceler.Parcels;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by miquel on 16/08/16.
 */
public class ListMessage_Adapter extends BaseAdapter implements View.OnClickListener {

    LayoutInflater layoutInflater;
    Activity activity;
    ListMessage_Adapter thisAdapter ;
    RealmResults<Message> messagesList;
    ListMessages_Activity listMessagesActivity;

    /**
     *
     * @param activity which we will inflate the layout
     */
    public ListMessage_Adapter(Activity activity) {
        listMessagesActivity = (ListMessages_Activity) activity;
        thisAdapter = this;
        layoutInflater = LayoutInflater.from(activity);
        this.activity = activity;
        loadMessagesList();
    }

    @Override
    public int getCount() {
        return messagesList.size();
    }

    @Override
    public Object getItem(int position) {
        return messagesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    // Si utilitzem el patró ViewHolder, el primer que farem és crear el model,
    // classe, per als objectes
    // que representen
    class ViewHolder {
        TextView date;
        TextView subject;
        ImageButton selectMessageButton;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            // Inflem el Layout de cada item
            convertView = layoutInflater.inflate(
                    R.layout.item_list_messages, null);
            holder = new ViewHolder();
            // Capturem els TextView
            holder.date = (TextView) convertView
                    .findViewById(R.id.date);
            holder.subject = (TextView) convertView
                    .findViewById(R.id.subject);
            holder.selectMessageButton = (ImageButton) convertView
                    .findViewById(R.id.selectButton);
            // Associem el viewholder,
            // la informació de l'estructura que hi ha d'haver dins el layout,
            // amb la vista que haurà de retornar aquest mètode.
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        LinearLayout nameList = (LinearLayout) convertView.findViewById(R.id.groupTextViews);
        nameList.setTag(position);
        nameList.setOnClickListener(this);

        ImageButton selectButton = (ImageButton) convertView.findViewById(R.id.selectButton);
        selectButton.setTag(position);
        selectButton.setOnClickListener(this);

        holder.date.setText(messagesList.get(position).getDate());
        holder.subject.setText(messagesList.get(position).getTitle());
        return convertView;
    }

    public void loadMessagesList(){
        messagesList = MessageDAO.findAll(activity);
    }

    /**
     * metodo en el que gestionamos que passa aundo se clica en el nombre, el boton delete o el boton de play de cada lista
     */
    @Override
    public void onClick(View v) {
        int position = (Integer) v.getTag();
        int id = v.getId();
        int id2 = R.id.groupTextViews;
        if (v.getId() == R.id.selectButton) {
            Boolean t = ListMessages_Activity.messagesList.contains(messagesList.get(position).getRealmId());
            if(!t){
                ListMessages_Activity.messagesList.add(messagesList.get(position).getRealmId());
                ImageButton b = (ImageButton) v;
                b.setImageResource(R.drawable.checked);
            } else {
                ListMessages_Activity.messagesList.remove(messagesList.get(position).getRealmId());
                ImageButton b = (ImageButton) v;
                b.setImageResource(R.drawable.defaultstate);
            }
        } else if (v.getId() == R.id.groupTextViews) {
            Message m = messagesList.get(position);

            RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(activity).build();
            Realm.setDefaultConfiguration(realmConfiguration);
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            m = realm.copyFromRealm(m);
            realm.commitTransaction();

            Intent intent = new Intent(activity, DetailMessage_Activity.class);
            intent.putExtra("message", Parcels.wrap(m));
            activity.startActivity(intent);
        }

    }

}
