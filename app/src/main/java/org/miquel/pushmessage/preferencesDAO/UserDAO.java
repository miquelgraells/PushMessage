package org.miquel.pushmessage.preferencesDAO;

import android.content.Context;
import android.content.SharedPreferences;

import org.miquel.pushmessage.R;

/**
 * Created by miquel on 24/08/16.
 */
public class UserDAO {
    public void updateUserName(String userID, Context ctx){
        SharedPreferences sharedpreferences = ctx.getSharedPreferences("myprefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("userID",userID);
        editor.commit();
    }

    public String getUserName(Context ctx){
        SharedPreferences sharedPref = ctx.getSharedPreferences("myprefs", Context.MODE_PRIVATE);
        String userID = sharedPref.getString("userID", null);
        return userID;
    }
}
