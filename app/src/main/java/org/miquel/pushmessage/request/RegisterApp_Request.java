package org.miquel.pushmessage.request;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.miquel.pushmessage.R;
import org.miquel.pushmessage.activities.RegisterUser_Activity;
import org.miquel.pushmessage.preferencesDAO.UserDAO;

import cz.msebera.android.httpclient.Header;


/**
 * Created by miquel on 18/08/16.
 *
 * Request to update set or update user ID
 */

public class RegisterApp_Request {

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void post(final RequestParams params, final Context context) {

        final ProgressDialog prgDialog = new ProgressDialog(context);
        prgDialog.setMessage("Updatin userID");
        prgDialog.setCancelable(true);
        prgDialog.show();

        client.post("http://pushmessage-mgraells.rhcloud.com/updateUserId", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // Root JSON in response is an dictionary i.e { "data : [ ... ] }
                // Handle resulting parsed JSON response here
                prgDialog.dismiss();
                try {
                    int status = response.getInt("status");
                    if(status == 200){
                        RegisterUser_Activity.userRegisterResult.setText("user id updated");
                        RegisterUser_Activity.showOldUserID.setText("Your userID is " + RegisterUser_Activity.uerNameTV.getText().toString());
                        UserDAO ud = new UserDAO();
                        ud.updateUserName(RegisterUser_Activity.uerNameTV.getText().toString(),context);
                    } else {
                        RegisterUser_Activity.userRegisterResult.setText("user id already exists");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });
    }

}
