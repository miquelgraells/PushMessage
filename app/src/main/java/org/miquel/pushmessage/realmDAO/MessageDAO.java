package org.miquel.pushmessage.realmDAO;

import android.content.Context;
import android.util.Log;

import org.miquel.pushmessage.model.Message;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by miquel on 16/08/16.
 */
public class MessageDAO {

    /**
     * inserts a route on db or updates in case that its already on db
     * @param message route object
     * @param context app context
     */
    public static void insert(Message message, Context context){
        // initzializing realm
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(context).build();
        Realm.setDefaultConfiguration(realmConfiguration);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Log.d("androiddebug",message.getRealmId());
        realm.copyToRealm(message);
        realm.commitTransaction();
    }

    public static void deleteArrayList(Context context, ArrayList<String> messageIds){
        // initzializing realm
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(context).build();
        Realm.setDefaultConfiguration(realmConfiguration);

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        for (int i = 0; i < messageIds.size(); i++) {
            RealmResults<Message> messages = realm.where(Message.class).findAll();
            RealmResults<Message> result2 = realm.where(Message.class)
                    .equalTo("realmId", messageIds.get(i))
                    .findAll();
            // delete route object
            result2.deleteAllFromRealm();
        }



        realm.commitTransaction();
    }

    /**
     *
     * @param context app context
     * @return list of all mesages on db
     */
    public static RealmResults<Message> findAll(Context context){
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(context).build();
        Realm.setDefaultConfiguration(realmConfiguration);

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        RealmResults<Message> messages = realm.where(Message.class).findAll();
        realm.commitTransaction();

        return messages;
    }

}
