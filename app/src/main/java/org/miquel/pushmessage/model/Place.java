package org.miquel.pushmessage.model;

import org.parceler.Parcel;

import io.realm.PlaceRealmProxy;
import io.realm.RealmObject;

/**
 * Created by miquel on 27/09/16.
 *
 * Poi class that represents a place.
 * Extends RealmObject to allow to be storage on local db
 * Implements Parcelable to allow pass object throw activities
 */

@Parcel(implementations = { PlaceRealmProxy.class },
        value = Parcel.Serialization.BEAN,
        analyze = { Place.class })
public class Place extends RealmObject {
    // lat coordinate
    double lat;
    // lng coordinate
    double lng;
    // obligate empty contructor to be saved on db using Realm
    public Place(){
    }
    public Place(double lat, double lng){
        this.lat = lat;
        this.lng = lng;
    }
    public double getLat() {
        return lat;
    }
    public double getLng() {
        return lng;
    }
    public void setLat(double lat) {
        this.lat = lat;
    }
    public void setLng(double lng) {
        this.lng = lng;
    }
    @Override
    public String toString() {
        return "Place{" +
                "lat=" + lat +
                ", lng=" + lng +
                '}';
    }
}
