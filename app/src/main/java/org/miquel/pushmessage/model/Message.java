package org.miquel.pushmessage.model;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.Calendar;

import io.realm.MessageRealmProxy;
import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by miquel on 16/08/16.
 *
 * Poi class that represents a message.
 * Extends RealmObject to allow to be storage on local db
 * Implements Parcelable to allow pass object throw activities
 */

@Parcel(implementations = { MessageRealmProxy.class },
        value = Parcel.Serialization.BEAN,
        analyze = { Message.class })
public class Message extends RealmObject {

    // id when Message is saved on local db
    String realmId;
    // message title
    private String title;
    // message text
    private String text;
    // message date
    private String date;
    // message places
    RealmList<Place> arrPlaces = new RealmList<>();

    // obligate empty contructor to be saved on db using Realm
    public Message (){

    }

    public Message (String title, String text){
        this.title = title;
        this.text = text;
        Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DATE);
        int month = c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int min = c.get(Calendar.MINUTE);

        this.date = String.format("%02d", day) + "/" + String.format("%02d", ++month) + "/" + year + " " + String.format("%02d", hour) + ":" + String.format("%02d", min);
    }

    public String getRealmId() {
        return realmId;
    }

    public void setRealmId(String realmId) {
        this.realmId = realmId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public RealmList<Place> getArrPlaces() {
        return arrPlaces;
    }

    @ParcelPropertyConverter(RealmListParcelConverter.class)
    public void setArrPlaces(RealmList<Place> arrPlaces) {
        this.arrPlaces = arrPlaces;
    }
}
