package org.miquel.pushmessage.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import org.miquel.pushmessage.R;
import org.miquel.pushmessage.adapter.ListMessage_Adapter;
import org.miquel.pushmessage.realmDAO.MessageDAO;
import java.util.ArrayList;

/**
 * Activity that shows a list of messages.
 */
public class ListMessages_Activity extends AppCompatActivity {

    public static ArrayList<String> messagesList = new ArrayList<String>();
    // to know if the activity is active so it can refersh the list of message when the app recives a notification
    public static boolean active = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_messages_);
        loadMessagesList();
    }

    @Override
    public void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadMessagesList();
        registerReceiver(mMessageReceiver, new IntentFilter("unique_name"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mMessageReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        messagesList.clear();
    }

    /*
     *  inflates a list and sets adapter
     */
    public void loadMessagesList(){
        ListMessage_Adapter adapter = new ListMessage_Adapter(this);
        ListView lv = (ListView) findViewById(R.id.messagesList);
        lv.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.delete:
                MessageDAO.deleteArrayList(this,messagesList);
                loadMessagesList();
                return true;
            case R.id.settings:
                Intent intent = new Intent(this, RegisterUser_Activity.class);
                startActivity(intent);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    /**
     * When the app recives a notifiaction then refresh the messages list automaticly
     */
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            loadMessagesList();
        }
    };

}
