package org.miquel.pushmessage.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.util.Linkify;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.miquel.pushmessage.R;
import org.miquel.pushmessage.model.Message;
import org.parceler.Parcels;

/**
 * Activity that shows the message sent from the web app
 */
public class DetailMessage_Activity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout mapsLayout;
    Message message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_message_);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        TextView tvText = (TextView) findViewById(R.id.text);
        TextView tvSubject = (TextView) findViewById(R.id.subject);

        Intent intent = getIntent();
        message = Parcels.unwrap(intent.getParcelableExtra("message"));
        mapsLayout = (LinearLayout) findViewById(R.id.mapsAttached);

        String text = "";
        String subject = "";
        // if message is null it means that the user has acces to this activity by clicking on the notification
        // not by clicking on the item from ListMessages_Activity
        if(message == null){
            text = intent.getStringExtra("messageText");
            subject = intent.getStringExtra("messageSubject");
            //TODO
            //get maps from intente when user cicks on notification
        } else {
            // getting the places and attaching them to buttons
            for(int i = 0; i < message.getArrPlaces().size(); i++){
                ImageButton mapButton = new ImageButton(this);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

                // leaving the first icon witoout margin
                if (i != 0)
                    params.setMargins(10,0,0,0);
                mapButton.setBackgroundDrawable( getResources().getDrawable(R.drawable.messagemap));

                mapButton.setLayoutParams(params);
                mapButton.setTag(i);
                mapButton.setOnClickListener(this);
                mapsLayout.addView(mapButton);
            }

            text = message.getText();
            subject = message.getTitle();
        }

        tvText.setText(text);
        tvSubject.setText(subject);
        // to make links clickable
        Linkify.addLinks(tvText, Linkify.WEB_URLS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        int position = (Integer) view.getTag();
        Intent intent = new Intent(this,MessagePlace.class);
        Double lat = message.getArrPlaces().get(position).getLat();
        Double lng = message.getArrPlaces().get(position).getLng();
        intent.putExtra("lat",lat);
        intent.putExtra("lng",lng);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
