package org.miquel.pushmessage.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.RequestParams;
import org.miquel.pushmessage.R;
import org.miquel.pushmessage.preferencesDAO.UserDAO;
import org.miquel.pushmessage.request.RegisterApp_Request;

/**
 * Activity that allows the user to set a user ID
 */
public class RegisterUser_Activity extends AppCompatActivity implements View.OnClickListener {

    public static EditText uerNameTV;
    public static TextView userRegisterResult, showOldUserID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_register_user_);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        UserDAO ud = new UserDAO();
        String userName = ud.getUserName(this);
        showOldUserID = (TextView) findViewById(R.id.showOldUserID);

        String userNotFound = String.valueOf(getResources().getString(R.string.user_not_found) +
                "\n ex : MyNameMyFirstname");
        String userID = String.valueOf(getResources().getString(R.string.your_userID_user)) + " " +  userName;
        userID = userName == null ? userNotFound : String.valueOf(getResources().getString(R.string.your_userID_user)) + " " +  userName;
        showOldUserID.setText(userID);

        uerNameTV = (EditText) findViewById(R.id.usernameTV);

        userRegisterResult = (TextView) findViewById(R.id.userRegisterResult);
        ImageButton registerBtn = (ImageButton) findViewById(R.id.registerBtn);
        registerBtn.setOnClickListener(this);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        String userName = uerNameTV.getText().toString();

        String token = FirebaseInstanceId.getInstance().getToken();
        RequestParams params = new RequestParams();
        params.put("token", token);
        params.put("user", userName);
        RegisterApp_Request.post(params, this);
    }


}
