package org.miquel.pushmessage.fcm;

/**
 * Created by miquel on 15/08/16.
 */
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.miquel.pushmessage.activities.DetailMessage_Activity;
import org.miquel.pushmessage.activities.ListMessages_Activity;
import org.miquel.pushmessage.R;
import org.miquel.pushmessage.model.Message;
import org.miquel.pushmessage.model.Place;
import org.miquel.pushmessage.realmDAO.MessageDAO;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;

import io.realm.RealmList;

/**
 * @author ton1n8o - antoniocarlos.dev@gmail.com on 6/13/16.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyGcmListenerService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Map<String, String> params = remoteMessage.getData();
        JSONObject messageJSON = new JSONObject(params);
        try {
            String image = messageJSON.getString("message");
            String title = messageJSON.getString("title");
            String sound = messageJSON.getString("sound");
            boolean hasPlaces = messageJSON.has("places");

            RealmList<Place> arrPlaces = new RealmList<Place>();
            if (hasPlaces) {
                String placesString = remoteMessage.getData().get("places");
                JSONArray placesJSON = new JSONArray(placesString);
                for (int i = 0; i < placesJSON.length(); i++) {
                    Double lat = Double.valueOf(placesJSON.getJSONObject(i).getString("lat"));
                    Double lng = Double.valueOf(placesJSON.getJSONObject(i).getString("lng"));
                    arrPlaces.add(new Place(lat, lng));
                }
            }

            String messageText = "";
            String obj = remoteMessage.getData().get("message");
            if (obj != null) {
                messageText = obj;
            }

            Message message = new Message(title,messageText);
            message.setArrPlaces(arrPlaces);
            message.setRealmId(String.valueOf(message.hashCode()));
            MessageDAO.insert(message,getApplicationContext());

            Intent intent = null;
            if(ListMessages_Activity.active) {
                intent = new Intent("unique_name");
                //put whatever data you want to send, if any
                intent.putExtra("message", messageText);
                //send broadcast
                sendBroadcast(intent);
            } else {
                //TODO maps to notification data

                this.sendNotification(new NotificationData(image, messageText, title, messageText, sound));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param notificationData GCM message received.
     */
    private void sendNotification(NotificationData notificationData) {

        Intent intent = new Intent(getApplicationContext(), DetailMessage_Activity.class);
        intent.putExtra("messageText", notificationData.getText());
        intent.putExtra("messageSubject", notificationData.getSummary());

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = null;
        try {
            notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(URLDecoder.decode(notificationData.getTitle(), "UTF-8"))
                    .setContentText(URLDecoder.decode(notificationData.getSummary(), "UTF-8"))
                    .setAutoCancel(true)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setContentIntent(pendingIntent);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (notificationBuilder != null) {
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(notificationData.getId(), notificationBuilder.build());

        }
    }
}
