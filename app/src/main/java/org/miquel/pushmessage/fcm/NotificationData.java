package org.miquel.pushmessage.fcm;

/**
 * Created by miquel on 15/08/16.
 */
public class NotificationData {

    private int id = hashCode();
    private String imageName;
    private String title;
    private String summary;
    private String text;
    private String sound;

    public NotificationData() {}

    public NotificationData(String imageName, String title, String summary, String text, String sound) {
        this.imageName = imageName;
        this.title = title;
        this.summary = summary;
        this.text = text;
        this.sound = sound;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
